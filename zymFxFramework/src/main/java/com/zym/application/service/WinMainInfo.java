package com.zym.application.service;

import javafx.stage.Window;

/**
 * QQ 165149324
 * 臧英明
 *
 * @author
 * @create 2021-02-21
 */
public class WinMainInfo {
    private double width;
    private double height;
    private double x;
    private double y;
    private Window window;

    public double getWidth() {
        return width;
    }

    public void setWidth(double width) {
        this.width = width;
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    public Window getWindow() {
        return window;
    }

    public void setWindow(Window window) {
        this.window = window;
    }
}
