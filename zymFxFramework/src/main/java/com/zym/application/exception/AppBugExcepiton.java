package com.zym.application.exception;

/**
 * QQ 165149324
 * 臧英明
 *
 * @author
 * @create 2021-02-03
 */
public class AppBugExcepiton extends RuntimeException {

    public AppBugExcepiton() {
    }

    public AppBugExcepiton(String message) {
        super(message);
    }

    public AppBugExcepiton(String message, Throwable cause) {
        super(message, cause);
    }

    public AppBugExcepiton(Throwable cause) {
        super(cause);
    }
}
