package com.zym.application.controller.tms.abnormal;

import com.zym.framework.combobox.ZymComboBox;
import com.zym.framework.datetimepicker.DateTimePicker;
import com.zym.framework.form.ZymField;
import com.zym.framework.utlis.FromUtils;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TextArea;

import java.net.URL;
import java.util.ResourceBundle;


/**
 * QQ 165149324
 * 臧英明
 *
 * @author
 * @create 2021-02-19
 */
public class TmsAbnormalFromController implements Initializable {
    @FXML
    @ZymField
    public ZymComboBox TYPE;
    @FXML
    @ZymField
    public DateTimePicker TIME;
    @FXML
    @ZymField
    public TextArea TEXT;


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        FromUtils.initDataBookDefault("AbnormalType",TYPE);
    }
}
