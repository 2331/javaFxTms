package com.zym.application.controller.base.taxrate;

import com.zym.framework.form.ZymField;
import com.zym.framework.textfield.NumberTextField;
import javafx.fxml.FXML;
import javafx.scene.control.TextField;


/**
 * QQ 165149324
 * 臧英明
 *
 * @author
 * @create 2021-02-19
 */
public class BaseTaxrateFromController {
    @FXML
    @ZymField
    public NumberTextField TAX_TATE;
    @FXML
    @ZymField
    private TextField NAME;
    @FXML
    @ZymField
    private TextField CODE;
    @FXML
    @ZymField
    private TextField DESCRIPTION;


}
