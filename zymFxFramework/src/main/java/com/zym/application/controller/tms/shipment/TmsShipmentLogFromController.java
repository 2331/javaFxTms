package com.zym.application.controller.tms.shipment;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.zym.application.column.BaseData;
import com.zym.application.model.ReturnModel;
import com.zym.application.session.Http;
import com.zym.application.session.HttpCall;
import com.zym.framework.combobox.ZymComboBox;
import com.zym.framework.form.ZymField;
import com.zym.framework.model.ParseModel;
import com.zym.framework.utlis.FromUtils;
import com.zym.framework.utlis.IviewParseUtils;
import com.zym.framework.winTool.WinTool;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.web.WebView;

import java.lang.reflect.Field;
import java.net.URL;
import java.util.*;

/**
 * QQ 165149324
 * 臧英明
 *
 * @author
 * @create 2021-02-05
 */
public class TmsShipmentLogFromController implements Initializable {

    @FXML    @ZymField
    public TextField CODE;
    @FXML    @ZymField
    public TextField DESPATCH_TIME;
    @FXML    @ZymField
    public TextField LICENSE_NUMBER;
    @FXML    @ZymField
    public TextField DRIVER;
    @FXML    @ZymField
    public TextField DRIVER_PHONE;
    @FXML    @ZymField
    public ZymComboBox TRACK_MODEL;
    @FXML    @ZymField
    public ZymComboBox TRANSPORT_TYPE;
    @FXML    @ZymField
    public TextArea DESCRIPTION;
    @FXML
    public TableView orders;
    @FXML
    public TableView products;
    @FXML
    public WebView maps;
    @FXML
    public TextArea track;
    @FXML
    public TabPane root;
    private WinTool winTool;
    private List<Field> fields = new ArrayList<>();
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        FromUtils.initDataBookDefault("TransportType",TRANSPORT_TYPE);
        FromUtils.initDataBookDefault("TrackModel",TRACK_MODEL);
        scanField(this);
        Map dataBook = new HashMap();
        dataBook.put("TASK","Istrue");
        dataBook.put("DELIVER","Istrue");
        dataBook.put("STATE","TmsOrderType");
        dataBook.put("TRACK_MODEL","TrackModel");
        dataBook.put("TRANSPORT_TYPE","TransportType");
        dataBook.put("LTL","LTL");
        ParseModel parseModel = IviewParseUtils.parseGrid(BaseData.SHIPMENT_LOG_ORDER, dataBook);
        orders.getColumns().setAll( parseModel.getTableColumns());
        ParseModel parseModel2 = IviewParseUtils.parseGrid(BaseData.SHIPMENT_LOG_ORDER, null);
        products.getColumns().setAll( parseModel2.getTableColumns());
        winTool = new WinTool("发运");
        winTool.setHeightWidth(696,1114);
        winTool.setNode(root);
    }
    private void scanField(Object controller){
        Field[] declaredFields = controller.getClass().getDeclaredFields();
        for (int i = 0; i <declaredFields.length ; i++) {
            Field field = declaredFields[i];
            ZymField annotation = field.getAnnotation(ZymField.class);
            if(annotation !=null){ //说明是对象字段
                fields.add(field);
            }
        }
    }
    public void openFrom(String id){
        Map map = new HashMap();
        FromUtils.clearFieid(fields,this);
        map.put("id",id);
        Http.post("/tmsshipment/getShipmenLog", map, new HttpCall() {
            @Override
            public void exe(ReturnModel returnModel) {
                JSONObject jsonObject = (JSONObject) returnModel.getObj();
                JSONObject shipment = jsonObject.getJSONObject("shipment");
                FromUtils.addFieldData(shipment,fields,TmsShipmentLogFromController.this);
                JSONArray p = jsonObject.getJSONArray("products");
                products.getItems().setAll(p);
                JSONArray o = jsonObject.getJSONArray("orders");
                orders.getItems().setAll(o);
                winTool.show();
            }
        });



    }
}
