package com.zym.application.controller.tms.shipment;

import com.alibaba.fastjson.JSONObject;
import com.zym.application.column.BaseData;
import com.zym.application.controller.tms.order.TmsOrderController;
import com.zym.application.model.ReturnModel;
import com.zym.application.session.Http;
import com.zym.application.session.HttpCall;
import com.zym.framework.combobox.ZymComboBox;
import com.zym.framework.datetimepicker.DateTimePicker;
import com.zym.framework.form.ZymField;
import com.zym.framework.model.ParseModel;
import com.zym.framework.notice.NoticeUtils;
import com.zym.framework.utlis.FromUtils;
import com.zym.framework.utlis.IviewParseUtils;
import com.zym.framework.winTool.WinTool;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;

import java.lang.reflect.Field;
import java.net.URL;
import java.util.*;

/**
 * QQ 165149324
 * 臧英明
 * 运单
 *
 * @author
 * @create 2021-03-16
 */
public class TmsShipmentController implements Initializable {
    @FXML @ZymField
    public TextField CODE;
    @FXML @ZymField
    public DateTimePicker DESPATCH_TIME;
    @FXML @ZymField
    public ZymComboBox TRANSPORT_TYPE;
    @FXML @ZymField
    public TextField LICENSE_NUMBER;
    @FXML @ZymField
    public TextField DRIVER;
    @FXML @ZymField
    public TextField DRIVER_PHONE;
    @FXML @ZymField
    public ZymComboBox TRACK_MODEL;
    @FXML @ZymField
    public TextField DESCRIPTION;
    @FXML
    private TableView table;
    @FXML
    private AnchorPane anchorPane;

    private TmsOrderController tmsOrderController;
    private WinTool winTool;

    private List<Field> fields = new ArrayList<>();

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        FromUtils.initDataBookDefault("TrackModel",TRACK_MODEL);
        FromUtils.initDataBookDefault("TransportType",TRANSPORT_TYPE);
        winTool = new WinTool("发运");
        Button button = new Button("发运");

        button.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {

                JSONObject par = new JSONObject();
                JSONObject fromData = getFromData();
                ObservableList items = table.getItems();
                par.put("jsonBean",fromData);
                par.put("jsonBeans",items);
                Http.post("'/tmsshipment/task", par, new HttpCall() {
                    @Override
                    public void exe(ReturnModel returnModel) {
                        if (returnModel.isSuceess()){
                            NoticeUtils.success(returnModel.getObj().toString());
                            tmsOrderController.queryList();
                            winTool.Hide();
                        }else {
                            NoticeUtils.error(returnModel.getObj().toString());
                        }
                    }
                });
            }
        });
        winTool.setButton(button);
        winTool.setHeightWidth(425,834);
        winTool.setNode(anchorPane);
        scanField(this);
        initGrid();
    }
    private JSONObject getFromData(){
      return   FromUtils.getFieldData(fields,this);
    }
    private void initGrid() {
        Map dataBook = new HashMap<>();
        dataBook.put("LTL","LTL");
        dataBook.put("TRANSPORT_TYPE","TransportType");
        ParseModel parseModel = IviewParseUtils.parseGrid(BaseData.SHIPMENT, dataBook);
        table.getColumns().setAll(parseModel.getTableColumns());
    }

    public void open(List list, TmsOrderController tmsOrderController){
        this.tmsOrderController = tmsOrderController;
        FromUtils.clearFieid(fields,this);
        TRANSPORT_TYPE.initValue();
        TRACK_MODEL.initValue();
        table.getItems().setAll(list);
        winTool.show();
    }
    private void scanField(Object controller){
        Field[] declaredFields = controller.getClass().getDeclaredFields();
        for (int i = 0; i <declaredFields.length ; i++) {
            Field field = declaredFields[i];
            ZymField annotation = field.getAnnotation(ZymField.class);
            if(annotation !=null){ //说明是对象字段
                fields.add(field);
            }
        }
    }
}
