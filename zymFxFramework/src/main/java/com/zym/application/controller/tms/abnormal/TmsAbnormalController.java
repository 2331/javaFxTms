package com.zym.application.controller.tms.abnormal;

import com.alibaba.fastjson.JSONObject;
import com.zym.application.column.BaseData;
import com.zym.application.utils.StringUtils;
import com.zym.framework.combobox.ZymComboBox;
import com.zym.framework.form.ZymCallBack;
import com.zym.framework.form.ZymForm;
import com.zym.framework.grid.ZymGrid;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.SplitPane;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.util.Callback;

import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;

/**
 * QQ 165149324
 * 臧英明
 *
 * @author
 * @create 2021-02-05
 */
public class TmsAbnormalController implements Initializable {
    @FXML
    public ZymComboBox select;
    private FXMLLoader fxmlLoader;
    private ZymGrid zymGrid;
    @FXML
    private SplitPane splitPane;
    private String fromXmlPath = "/view/tms/abnormal/tms-abnormal-from.fxml";
    private ZymForm zymForm;
    @FXML
    private TextField itemName;
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        this.fxmlLoader = ZymGrid.create();
        this.zymGrid = fxmlLoader.getController();
        Map databook = new HashMap();
        databook.put("TYPE","AbnormalType");
        zymGrid.initView("/tmsabnormal/listToOrg",BaseData.TMS_ABNORMAL,databook);
        zymGrid.setWinHeight(700);

        Parent root = this.fxmlLoader.getRoot();
        splitPane.getItems().add(root);
        this.zymForm = new ZymForm("/tmsabnormal/saveToOrg","/tmsabnormal/get",fromXmlPath,"异常管理");
        zymForm.setFormCallback(new ZymCallBack() {
            @Override
            public void submitEnd() {
                queryList();
            }
        });
        zymGrid.setRowFactory(new Callback<TableView, TableRow>() {
            @Override
            public TableRow call(TableView param) {
                TableRow<JSONObject> row = new TableRow<JSONObject>();
                row.setOnMouseClicked(event -> {
                    if (event.getClickCount() == 2 && (! row.isEmpty()) ) {
                        JSONObject jsonObject = row.getItem();
                        String id = jsonObject.getString("ID");
                        zymForm.open(id);
                    }
                });
                return row ;
            }
        });
        select.getItems().setAll( zymGrid.getSelectModels());
        select.initValue();

    }
    public void queryList(){
        Map map= new HashMap();
        if(StringUtils.isNotEmpty(itemName.getText())){
            map.put("sqlWhereNameToLike",select.getValue().getId());
            map.put("sqlWhereValueToLike",itemName.getText());
        }
        zymGrid.query(map);
    }
    public void open(){
        zymForm.open(null);
    }


}
