package com.zym.application.controller.base.zone;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.zym.application.column.BaseData;
import com.zym.application.model.ReturnModel;
import com.zym.application.session.Http;
import com.zym.application.session.HttpCall;
import com.zym.application.utils.StringUtils;
import com.zym.framework.grid.ZymGrid;
import com.zym.framework.model.SelectModel;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.SplitPane;
import javafx.scene.control.TreeCell;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import javafx.scene.input.MouseEvent;
import javafx.util.Callback;

import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;

/**
 * QQ 165149324
 * 臧英明
 *
 * @author
 * @create 2021-02-05
 */
public class BaseZoneController implements Initializable {
    public TreeView treeView;
    private FXMLLoader fxmlLoader;
    private ZymGrid zymGrid;
    @FXML
    private SplitPane splitPane;
//    private String fromXmlPath = "/view/base/zone/base-zone-from.fxml";
//    private ZymForm zymForm;
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        this.fxmlLoader = ZymGrid.create();
        this.zymGrid = fxmlLoader.getController();
        zymGrid.initView("/baseZone/getList",BaseData.BASE_ZONE);
        zymGrid.setWinHeight(700);

        Parent root = this.fxmlLoader.getRoot();
        splitPane.getItems().add(root);
//        this.zymForm = new ZymForm("/unitManage/save","/unitManage/get",fromXmlPath,"单位管理");
//        zymGrid.setRowFactory(new Callback<TableView, TableRow>() {
//            @Override
//            public TableRow call(TableView param) {
//                TableRow<JSONObject> row = new TableRow<JSONObject>();
//                row.setOnMouseClicked(event -> {
//                    if (event.getClickCount() == 2 && (! row.isEmpty()) ) {
//                        JSONObject jsonObject = row.getItem();
//                        String id = jsonObject.getString("ID");
//                        zymForm.open(id);
//                    }
//                });
//                return row ;
//            }
//        });
        //加载树
        treeView.setCellFactory(new Callback<TreeView<SelectModel>,TreeCell<SelectModel>>(){
            @Override
            public TreeCell<SelectModel> call(TreeView<SelectModel> p) {
                return new TreeCell<SelectModel>(){
                    @Override
                    public void updateItem(SelectModel item, boolean empty) {
                        super.updateItem(item, empty);
                        if (empty) {
                            setText(null);
                            setGraphic(null);
                        } else {
                            setId(item.getId());
                            setText(item.getValue());
                            setGraphic( getTreeItem().getGraphic());
                        }
                    }
                };
            }
        });
        getRootTree();
        //注册加载
        treeView.setOnMouseClicked(new EventHandler<MouseEvent>()
        {
            @Override
            public void handle(MouseEvent mouseEvent)
            {
                if(mouseEvent.getClickCount() == 2)
                {
                    TreeItem<SelectModel> item = (TreeItem<SelectModel>) treeView.getSelectionModel().getSelectedItem();
                    getLeftTree(item);
                    queryList(item.getValue().getId());
                }
            }
        });
        queryList(null);
    }
    public void getLeftTree(TreeItem<SelectModel> item){
        SelectModel value = item.getValue();
        if(StringUtils.isNotEmpty(value.getId())){
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("id",value.getId());
            Http.post("/baseZone/getZoneTree", jsonObject, new HttpCall() {
                @Override
                public void exe(ReturnModel returnModel) {
                    Object obj = returnModel.getObj();
                    if(obj != null){
                        JSONArray jsonArray = (JSONArray) obj;
                        for (int i = 0; i < jsonArray.size(); i++) {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);
                            TreeItem treeItem = new TreeItem();
                            SelectModel itemModel = new SelectModel();
                            itemModel.setId(jsonObject.getString("id"));
                            itemModel.setValue(jsonObject.getString("title"));
                            treeItem.setValue(itemModel);
                            item.getChildren().add(treeItem);
                        }
                        item.setExpanded(true);
                    }
                }
            });
        }
    }
    public void getRootTree(){
        Http.post("/baseZone/getZoneTree", null, new HttpCall() {
            @Override
            public void exe(ReturnModel returnModel) {
                Object obj = returnModel.getObj();
                if(obj != null){
                    TreeItem<SelectModel> root = new TreeItem();
                    SelectModel rootModel = new SelectModel();
                    rootModel.setId("ROOT");
                    rootModel.setValue("中华人民共和国");
                    root.setValue(rootModel);
                    root.setExpanded(true);
                    JSONArray jsonArray = (JSONArray) obj;
                    for (int i = 0; i < jsonArray.size(); i++) {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        TreeItem treeItem = new TreeItem();
                        SelectModel itemModel = new SelectModel();
                        itemModel.setId(jsonObject.getString("id"));
                        itemModel.setValue(jsonObject.getString("title"));
                        treeItem.setValue(itemModel);
                        root.getChildren().add(treeItem);
                    }
                    treeView.setRoot(root);
                }
            }
        });

    }
    public void queryList(String id){
        Map map = new HashMap();
        map.put("id",id);
        long l = System.currentTimeMillis();
        Http.post(zymGrid.getUrl(), map, new HttpCall() {
            @Override
            public void exe(ReturnModel returnModel) {
                Object obj = returnModel.getObj();
                if(obj != null){
                    JSONObject objs = (JSONObject) obj;
                    zymGrid.addData(objs,l);
                }
            }
        });
    }
//    public void open(){
//        zymForm.open(null);
//    }

//    public void del(){
//        List<String>  ids= zymGrid.getSelectId();
//        if(ids.size() == 0){
//            NoticeUtils.error("请勾选要更改的数据");
//            return;
//        }
//        String str = "条数据执行删除？";
//        if(!AlertUtils.confirmYesNoConfirmation("通知","您确定要对这"+ids.size()+str)){
//            return;
//        }
//        JSONObject par = new JSONObject();
//        par.put("ids",ids);
//        Http.post("/unitManage/delete", par, new HttpCall() {
//            @Override
//            public void exe(ReturnModel returnModel) {
//                if (returnModel.isSuceess()){
//                    NoticeUtils.success(returnModel.getObj().toString());
//                    queryList();
//                }else {
//                    NoticeUtils.error(returnModel.getObj().toString());
//                }
//
//            }
//        });
//    }
}
