package com.zym.application.controller.base.kpi;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.zym.application.column.BaseData;
import com.zym.application.model.ReturnModel;
import com.zym.application.service.BaseDataSetvice;
import com.zym.application.session.Http;
import com.zym.application.session.HttpCall;
import com.zym.application.utils.StringUtils;
import com.zym.framework.alert.AlertUtils;
import com.zym.framework.combobox.ZymComboBox;
import com.zym.framework.form.ZymCallBack;
import com.zym.framework.form.ZymForm;
import com.zym.framework.grid.ZymGrid;
import com.zym.framework.model.SelectModel;
import com.zym.framework.notice.NoticeUtils;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.*;
import javafx.util.Callback;

import java.net.URL;
import java.util.*;

/**
 * QQ 165149324
 * 臧英明
 *
 * @author
 * @create 2021-02-05
 */
public class BaseKpiController implements Initializable {
    private FXMLLoader fxmlLoader;
    private ZymGrid zymGrid;
    @FXML
    private SplitPane splitPane;
    private String fromXmlPath = "/view/base/kpi/base-kpi-from.fxml";
    private ZymForm zymForm;
    @FXML
    private TextField itemName;
    @FXML
    private ZymComboBox select;
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        this.fxmlLoader = ZymGrid.create();
        this.zymGrid = fxmlLoader.getController();
        BaseDataSetvice instance = BaseDataSetvice.getInstance();
        JSONArray arrayData = instance.getArrayData("/item/init");
        List<SelectModel> list = BaseDataSetvice.toSelectModelList(arrayData, "ID", "NAME");
        Map databook = new HashMap();
        databook.put("COUNT_TYPE","KpiType");
        databook.put("BASE_ITEM_ID", new Callback<TableColumn.CellDataFeatures<Map, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<Map, String> param) {
                Map value = param.getValue();
                Object bit = value.get("BASE_ITEM_ID");
                if(bit != null){
                    String bitStr = bit.toString();
                    for (int i = 0; i < list.size(); i++) {
                        if(list.get(i).getId().equals(bitStr)){
                            return new ReadOnlyObjectWrapper(list.get(i).getValue());
                        }
                    }
                }
                return null;
            }
        });
        zymGrid.initView("/kpi/list",BaseData.BASE_KPI,databook);
        zymGrid.setWinHeight(700);

        Parent root = this.fxmlLoader.getRoot();
        splitPane.getItems().add(root);
        this.zymForm = new ZymForm("/kpi/save","/kpi/get",fromXmlPath,"时效管理");
        zymForm.setFormCallback(new ZymCallBack() {
            @Override
            public void submitEnd() {
                queryList();
            }
        });
        zymGrid.setRowFactory(new Callback<TableView, TableRow>() {
            @Override
            public TableRow call(TableView param) {
                TableRow<JSONObject> row = new TableRow<JSONObject>();
                row.setOnMouseClicked(event -> {
                    if (event.getClickCount() == 2 && (! row.isEmpty()) ) {
                        JSONObject jsonObject = row.getItem();
                        String id = jsonObject.getString("ID");
                        zymForm.open(id);
                    }
                });
                return row ;
            }
        });
        select.getItems().setAll( zymGrid.getSelectModels());
        select.initValue();
    }
    public void queryList(){
        Map map= new HashMap();
        if(StringUtils.isNotEmpty(itemName.getText())){
            map.put("sqlWhereNameToLike",select.getValue().getId());
            map.put("sqlWhereValueToLike",itemName.getText());
        }
        zymGrid.query(map);
    }
    public void open(){
        zymForm.open(null);
    }

    public void del(){
        List<String>  ids= zymGrid.getSelectId();
        if(ids.size() == 0){
            NoticeUtils.error("请勾选要更改的数据");
            return;
        }
        String str = "条数据执行删除？";
        if(!AlertUtils.confirmYesNoConfirmation("通知","您确定要对这"+ids.size()+str)){
            return;
        }
        JSONObject par = new JSONObject();
        par.put("ids",ids);
        Http.post("/kpi/delete", par, new HttpCall() {
            @Override
            public void exe(ReturnModel returnModel) {
                if (returnModel.isSuceess()){
                    NoticeUtils.success(returnModel.getObj().toString());
                    queryList();
                }else {
                    NoticeUtils.error(returnModel.getObj().toString());
                }

            }
        });
    }
}
