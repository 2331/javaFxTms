package com.zym.application.controller.sys.user;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.zym.application.column.BaseData;
import com.zym.application.model.ReturnModel;
import com.zym.application.session.Http;
import com.zym.application.session.HttpCall;
import com.zym.application.utils.StringUtils;
import com.zym.framework.CallBack;
import com.zym.framework.alert.AlertUtils;
import com.zym.framework.combobox.ZymComboBox;
import com.zym.framework.form.ZymCallBack;
import com.zym.framework.form.ZymForm;
import com.zym.framework.grid.ZymGrid;
import com.zym.framework.model.SelectModel;
import com.zym.framework.notice.NoticeUtils;
import com.zym.framework.winTool.WinCheck;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.SplitPane;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.util.Callback;

import java.net.URL;
import java.util.*;

/**
 * QQ 165149324
 * 臧英明
 *
 * @author
 * @create 2021-02-05
 */
public class TmsUserController implements Initializable {
    @FXML
    public ZymComboBox select;
    private FXMLLoader fxmlLoader;
    private ZymGrid zymGrid;
    @FXML
    private SplitPane splitPane;
    private String fromXmlPath = "/view/tms/user/tms-user-from.fxml";
    private ZymForm zymForm;
    private WinCheck winCheck;
    @FXML
    private TextField itemName;
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        this.fxmlLoader = ZymGrid.create();
        this.zymGrid = fxmlLoader.getController();
        Map databook = new HashMap();
        databook.put("STATE","CommState");
        zymGrid.initView("/tmsuser/list",BaseData.USER,databook);
        zymGrid.setWinHeight(700);

        Parent root = this.fxmlLoader.getRoot();
        splitPane.getItems().add(root);
        this.zymForm = new ZymForm("/tmsuser/save","/tmsuser/get",fromXmlPath,"用户管理");
        zymForm.setFormCallback(new ZymCallBack() {
            @Override
            public void submitEnd() {
                queryList();
            }
        });
        zymGrid.setRowFactory(new Callback<TableView, TableRow>() {
            @Override
            public TableRow call(TableView param) {
                TableRow<JSONObject> row = new TableRow<JSONObject>();
                row.setOnMouseClicked(event -> {
                    if (event.getClickCount() == 2 && (! row.isEmpty()) ) {
                        JSONObject jsonObject = row.getItem();
                        String id = jsonObject.getString("ID");
                        zymForm.open(id);
                    }
                });
                return row ;
            }
        });
        select.getItems().setAll( zymGrid.getSelectModels());
        select.initValue();

        winCheck =  new WinCheck(new CallBack() {
            @Override
            public void run(JSONObject jsonObject) {
                AlertUtils.error("C/S此功能暂未实现");
            }
        });
    }
    public void queryList(){
        Map map= new HashMap();
        if(StringUtils.isNotEmpty(itemName.getText())){
            map.put("sqlWhereNameToLike",select.getValue().getId());
            map.put("sqlWhereValueToLike",itemName.getText());
        }
        zymGrid.query(map);
    }
    public void open(){
        zymForm.open(null);
    }

    public void audit(){
        updateState(2);
    }
    public void unAudit(){

        updateState(0);
    }

    private void updateState(int state){
        List<String>  ids= zymGrid.getSelectId();
        if(ids.size() == 0){
            NoticeUtils.error("请勾选要更改的数据");
            return;
        }
        String str = "条数据审核？";
        if(state == 0){
            str= "条数据失效？";
        }
        if(!AlertUtils.confirmYesNoConfirmation("通知","您确定要对这"+ids.size()+str)){
            return;
        }


        JSONObject par = new JSONObject();
        par.put("ids",ids);
        par.put("name",state);
        Http.post("/tmsuser/updateState", par, new HttpCall() {
            @Override
            public void exe(ReturnModel returnModel) {
                if (returnModel.isSuceess()){
                    NoticeUtils.success(returnModel.getObj().toString());
                    queryList();
                }else {
                    NoticeUtils.error(returnModel.getObj().toString());
                }

            }
        });
    }
    public void getOrg(){
        List<String>  ids= zymGrid.getSelectId();
        if(ids.size() != 1){
            NoticeUtils.error("只能选择一个用户进行修改");
            return;
        }
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("id",ids.get(0));
        Http.post("/tmsuser/getOrgRoleData", jsonObject, new HttpCall() {
            @Override
            public void exe(ReturnModel returnModel) {
                JSONArray obj = (JSONArray) returnModel.getObj();
                List checks = new ArrayList();
                for (int i = 0; i <obj.size() ; i++) {
                    JSONObject data = obj.getJSONObject(i);
                    SelectModel selectModel = new SelectModel();
                    selectModel.setId(data.getString("ID"));
                    selectModel.setValue(data.getString("NAME"));
                    checks.add(selectModel);
                }
                winCheck.setAll(checks);
                winCheck.show();
            }
        });
    }
    public void getRole(){
        List<String>  ids= zymGrid.getSelectId();
        if(ids.size() != 1){
            NoticeUtils.error("只能选择一个用户进行修改");
            return;
        }
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("id",ids.get(0));
        Http.post("/tmsuser/getUserRoleData", jsonObject, new HttpCall() {
            @Override
            public void exe(ReturnModel returnModel) {
                JSONArray obj = (JSONArray) returnModel.getObj();
                List checks = new ArrayList();
                for (int i = 0; i <obj.size() ; i++) {
                    JSONObject data = obj.getJSONObject(i);
                    SelectModel selectModel = new SelectModel();
                    selectModel.setId(data.getString("ID"));
                    selectModel.setValue(data.getString("NAME"));
                    checks.add(selectModel);
                }
                winCheck.setAll(checks);
                winCheck.show();
            }
        });
    }
}
