package com.zym.application.controller.sys.org;

import com.zym.framework.form.ZymField;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TextField;

import java.net.URL;
import java.util.ResourceBundle;


/**
 * QQ 165149324
 * 臧英明
 *
 * @author
 * @create 2021-02-19
 */
public class TmsOrgFromController implements Initializable {
    @FXML @ZymField
    public TextField NAME;
    @FXML @ZymField
    public TextField PHONE;
    @FXML @ZymField
    public TextField DESCRIPTION;
    @FXML @ZymField
    public TextField PERSION;
    @FXML @ZymField
    public TextField CODE;


    @Override
    public void initialize(URL location, ResourceBundle resources) {
    }
}
