package com.zym.application.controller.base.productType;

import com.zym.framework.form.ZymField;
import javafx.fxml.FXML;
import javafx.scene.control.TextField;


/**
 * QQ 165149324
 * 臧英明
 *
 * @author
 * @create 2021-02-19
 */
public class BaseProductTypeFromController {
    @FXML
    @ZymField
    private TextField NAME;
    @FXML
    @ZymField
    private TextField CODE;
    @FXML
    @ZymField
    private TextField DESCRIPTION;


}
