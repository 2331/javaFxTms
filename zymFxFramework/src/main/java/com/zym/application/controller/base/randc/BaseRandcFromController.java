package com.zym.application.controller.base.randc;

import com.zym.application.column.BaseData;
import com.zym.framework.area.Area;
import com.zym.framework.combobox.AddressAutoComplete;
import com.zym.framework.combobox.AutoComplete;
import com.zym.framework.combobox.QueryComboBox;
import com.zym.framework.form.ZymField;
import com.zym.framework.model.ParseModel;
import com.zym.framework.utlis.IviewParseUtils;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;

import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;


/**
 * QQ 165149324
 * 臧英明
 *
 * @author
 * @create 2021-02-19
 */
public class BaseRandcFromController implements Initializable {
    @FXML
    @ZymField
    public QueryComboBox BASE_CUSTOMER_SUPPLIER_ID;
    @FXML
    @ZymField
    public TextField PERSION;
    @FXML
    @ZymField
    public TextField PHONE;
    @FXML
    @ZymField
    public ComboBox ADDRESS;
    @FXML
    @ZymField
    public Area AREA;
    @FXML
    @ZymField
    private TextField NAME;
    @FXML
    @ZymField
    private TextField DESCRIPTION;


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        new AddressAutoComplete(new AutoComplete<>(ADDRESS));
        ParseModel parseModel = IviewParseUtils.parseGrid(BaseData.CSQUERY, null);
        Map map = new HashMap<>();
        map.put("name","CUSTOMER");
        BASE_CUSTOMER_SUPPLIER_ID.initGrid(parseModel.getTableColumns(),"NAME","/basecustomersupplier/initTableSelect",map,"ID","NAME");
    }
}
