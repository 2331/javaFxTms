package com.zym.application.model;

import com.alibaba.fastjson.JSONObject;

/**
 * QQ 165149324
 * 臧英明
 * 固定化返回序列化类~
 * @author
 * @create 2021-01-30
 */
public class ReturnModel {
    //返回状态
    private String result;
    private boolean update;
    //返回内容
    private Object obj;
    //返回数据
    private Object bean;

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public boolean isUpdate() {
        return update;
    }

    public void setUpdate(boolean update) {
        this.update = update;
    }


    public Object getObj() {
        return obj;
    }

    public void setObj(Object obj) {
        this.obj = obj;
    }

    public Object getBean() {
        return bean;
    }

    public void setBean(Object bean) {
        this.bean = bean;
    }

    public boolean isSuceess() {
        return "success".equals(this.result);
    }
    public boolean isFailed(){
        return  "failed".equals(this.result);
    }
    public boolean isValid(){return  "valid".equals(this.result);}
    public JSONObject getObjToJSONObject(){
        return JSONObject.parseObject(obj.toString());
    }
    public String ObjToString(){
        return JSONObject.toJSONString(obj);
    }
}
