package com.zym.application.dataBook;

import com.alibaba.fastjson.JSONObject;
import com.zym.application.config.SystemConfig;

/**
 * QQ 165149324
 * 臧英明
 *
 * @author
 * @create 2021-02-23
 */
public class DataBookUtils {
    private static SystemConfig systemConfig = SystemConfig.getInstance();
    public static JSONObject getDataBookValue(String key){
        return systemConfig.getDataBook().getJSONObject(key);
    }
}
