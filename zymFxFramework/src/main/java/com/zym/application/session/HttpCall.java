package com.zym.application.session;

import com.zym.application.model.ReturnModel;

/**
 * QQ 165149324
 * 臧英明
 *
 * @author
 * @create 2021-02-22
 */
public interface HttpCall {
    void exe(ReturnModel returnModel);
}
