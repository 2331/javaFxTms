package com.zym.application;

import com.zym.application.config.SystemConfig;
import com.zym.application.service.PageService;
import com.zym.application.utils.SystemUtils;
import com.zym.framework.alert.AlertUtils;
import javafx.application.Application;
import javafx.event.Event;
import javafx.stage.Stage;

public class Main extends Application {
    @Override
    public void start(Stage primaryStage) throws Exception{
        systemInit(primaryStage);
        primaryStage.setOnCloseRequest(this::confirmExit);
        PageService init = PageService.init(primaryStage);
        init.goLogin();

    }
    private void systemInit(Stage primaryStage){
        //初始化XML
        SystemConfig instance = SystemConfig.getInstance();
        System.out.println("初始化");
        instance.setStage(primaryStage);
        instance.initXmlConfig();//

    }
    public static void main(String[] args) {
        launch(args);
    }

    private void confirmExit(Event event) {
        boolean boos = AlertUtils.confirmYesNoWarning("退出应用", "确定要退出吗？");
        if (boos) {
                SystemUtils.systemOut();
         }else {
            event.consume();
        }
    }

}
