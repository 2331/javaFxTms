package com.zym.application.utils;

/**
 * QQ 165149324
 * 臧英明
 * 税率计算器
 * @author
 * @create 2021-03-09
 */
public abstract class RateUtils {
    public static double countRate(double amount,double taxrate){
        return MathExtend.round(amount/(1+taxrate)*taxrate,3);
    }
}
