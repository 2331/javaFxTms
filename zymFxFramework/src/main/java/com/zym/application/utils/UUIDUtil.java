package com.zym.application.utils;

import java.util.UUID;

/**
 *QQ 165149324
 *
 * UUID生成工具
 *
 * @author 臧英明
 * @create 2017-11-19
 */
public abstract class UUIDUtil {
    /**
     * 获得一个32位UUID
     * @return
     */
    public static String getUuidTo32() {
        String uuid = UUID.randomUUID().toString().trim().replaceAll("-", "");
        return uuid.toString();
    }
}
