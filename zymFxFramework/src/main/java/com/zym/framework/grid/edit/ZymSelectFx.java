package com.zym.framework.grid.edit;

import com.zym.framework.combobox.ZymComboBox;
import com.zym.framework.model.SelectModel;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.ObservableList;
import javafx.geometry.Pos;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.util.Callback;

import java.util.List;
import java.util.Map;

/**
 * QQ 165149324
 * 臧英明
 *
 * @author
 * @create 2021-03-09
 */
public class ZymSelectFx implements Callback<TableColumn<Map<String, String>, Object>, TableCell<Map<String, String>, Object>> {
    private String key;
    private List<SelectModel> selectModels;
    private ChangeListener changeListener;

    public void setChangeListener(ChangeListener changeListener) {
        this.changeListener = changeListener;
    }
    public ZymSelectFx(String key,List items){
        this.key = key;
        this.selectModels = items;
    }

    @Override
    public TableCell<Map<String, String>, Object> call(TableColumn<Map<String, String>, Object> param) {

        TableCell<Map<String, String>, Object> cell = new TableCell<Map<String, String>, Object>() {
            protected  void   updateItem(Object item, boolean empty) {
                super.updateItem(item, empty);
                if (empty) {
                    this.setGraphic(null);
                }else {
                    ZymComboBox comboBox = new ZymComboBox();
                    comboBox.setStyle("-fx-border-radius: 0px;-fx-background-radius: 0;");
                    ObservableList<Map<String, String>> tableData = param.getTableView().getItems();
                    comboBox.getItems().setAll(selectModels);
                    double prefWidth = param.getPrefWidth();
                    double width = param.getWidth();
                    comboBox.setPrefWidth(prefWidth);
                    this.setGraphic(comboBox);
                    Object o = ((Map) tableData.get(getIndex())).get(key);
                    if(o != null){
                        if(o instanceof String){
                            String value = (String) o;
                            for (int i = 0; i <selectModels.size() ; i++) {
                                SelectModel selectModel = selectModels.get(i);
                                if(selectModel.getId().equals(value)){
                                    comboBox.setValue(selectModel);
                                    break;
                                }
                            }
                        }
                    }
                    comboBox.focusedProperty().addListener(new ChangeListener<Object>() {
                        @Override
                        public void changed(ObservableValue<? extends Object> observable, Object oldValue, Object newValue) {
                                SelectModel value = comboBox.getValue();
                                if(value != null){
                                    if (changeListener!=null){
                                        changeListener.changed(null,getIndex(), ((Map)tableData.get(getIndex())));
                                    }
                                    ((Map)tableData.get(getIndex())).put(key, value.getId());
                                }
                        }
                    });
                }
            }
        };
        cell.setAlignment(Pos.CENTER);
        return cell;
    }
}