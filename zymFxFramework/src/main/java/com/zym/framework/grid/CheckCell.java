package com.zym.framework.grid;

import com.zym.framework.constants.Grid;
import javafx.collections.ObservableList;
import javafx.geometry.Pos;
import javafx.scene.control.CheckBox;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.util.Callback;

import java.util.Map;

/**
 * QQ 165149324
 * 臧英明
 *
 * @author
 * @create 2021-02-19
 */
public class CheckCell implements Callback<TableColumn<Map<String, Boolean>, Boolean>, TableCell<Map<String, Boolean>, Boolean>> {


    @Override
    public TableCell<Map<String, Boolean>, Boolean> call(TableColumn<Map<String, Boolean>, Boolean> param) {
        TableCell<Map<String, Boolean>, Boolean> cell = new TableCell<Map<String, Boolean>, Boolean>() {
            protected void updateItem(Boolean item, boolean empty) {
                super.updateItem(item, empty);
                this.setGraphic(null);
                if (!empty) {
                    ObservableList<Map<String, Boolean>> tableData = param.getTableView().getItems();
                    CheckBox checkBox = new CheckBox();
                    Boolean $_select = tableData.get(this.getIndex()).get(Grid.SELECT);
                    if($_select == null){
                        $_select = false;
                    }
                    checkBox.setSelected($_select);
                    checkBox.selectedProperty().addListener((obVal, oldVal, newVal) -> {
                        ((Map)tableData.get(this.getIndex())).put(Grid.SELECT, newVal);

                    });
                    this.setGraphic(checkBox);
                }
            }
        };
        cell.setAlignment(Pos.CENTER);
        return cell;
    }
}
