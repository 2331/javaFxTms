package com.zym.framework.grid.edit;


import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.ObservableList;
import javafx.geometry.Pos;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TextField;
import javafx.util.Callback;

import java.util.Map;

/**
 * QQ 165149324
 * 臧英明
 * 对这个复写
 *
 * @author
 * @create 2021-03-06
 */
public class ZymTextFieldTableFX implements Callback<TableColumn<Map<String, String>, Object>, TableCell<Map<String, String>, Object>> {
    private String key;
    public ZymTextFieldTableFX(String key){
        this.key = key;
    }

    @Override
    public TableCell<Map<String, String>, Object> call(TableColumn<Map<String, String>, Object> param) {

        TableCell<Map<String, String>, Object> cell = new TableCell<Map<String, String>, Object>() {
            protected  void   updateItem(Object item, boolean empty) {
                super.updateItem(item, empty);
                if (empty) {
                    this.setGraphic(null);
                }else {
                    TextField textField = new TextField();
                    textField.setStyle("-fx-border-radius: 0px;-fx-background-radius: 0;");
                    ObservableList<Map<String, String>> tableData = param.getTableView().getItems();
                    this.setGraphic(textField);
                    Object o = ((Map) tableData.get(getIndex())).get(key);
                    if(o != null){
                        textField.setText(o.toString());
                    }
                    textField.focusedProperty().addListener(new ChangeListener<Object>() {
                        @Override
                        public void changed(ObservableValue<? extends Object> observable, Object oldValue, Object newValue) {
                            ((Map)tableData.get(getIndex())).put(key, textField.getText());
                        }
                    });
                }
            }
        };
        cell.setAlignment(Pos.CENTER);
        return cell;
    }

}
