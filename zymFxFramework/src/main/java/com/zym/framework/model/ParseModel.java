package com.zym.framework.model;

import javafx.scene.control.TableColumn;

import java.util.List;

/**
 * QQ 165149324
 * 臧英明
 *
 * @author
 * @create 2021-02-25
 */
public class ParseModel {
    private List<TableColumn> tableColumns;
    private List<SelectModel> selectModels;

    public List<TableColumn> getTableColumns() {
        return tableColumns;
    }

    public void setTableColumns(List<TableColumn> tableColumns) {
        this.tableColumns = tableColumns;
    }

    public List<SelectModel> getSelectModels() {
        return selectModels;
    }

    public void setSelectModels(List<SelectModel> selectModels) {
        this.selectModels = selectModels;
    }
}
