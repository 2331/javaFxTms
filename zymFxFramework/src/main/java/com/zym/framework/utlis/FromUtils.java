package com.zym.framework.utlis;

import com.alibaba.fastjson.JSONObject;
import com.zym.application.config.SystemConfig;
import com.zym.application.exception.AppBugExcepiton;
import com.zym.application.utils.StringUtils;
import com.zym.framework.combobox.QueryComboBox;
import com.zym.framework.combobox.ZymComboBox;
import com.zym.framework.datetimepicker.DateTimePicker;
import com.zym.framework.model.SelectModel;
import com.zym.framework.textfield.NumberTextField;
import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.control.*;
import javafx.scene.layout.Pane;

import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 * QQ 165149324
 * 臧英明
 *
 * @author
 * @create 2021-02-21
 */
public abstract class FromUtils {
    private static final JSONObject dataBook = SystemConfig.getInstance().getDataBook();
    public static JSONObject getFieldData(List<Field> list, Object obj) {
        JSONObject jsonObject = new JSONObject();
        for (int i = 0; i < list.size(); i++) {
            Field field = list.get(i);
            field.setAccessible(true);
            Object o = null;
            try {
                o = field.get(obj);
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
            if(o instanceof TextInputControl){
                TextInputControl textField = (TextInputControl) o;
                jsonObject.put(textField.getId(),textField.getText());
            }else if(o instanceof QueryComboBox){
                QueryComboBox control = (QueryComboBox) o;
                SelectModel selectData = control.getSelectData();
                jsonObject.put(control.getId(),selectData.getId());
            }else if(o instanceof ComboBoxBase){
                ComboBoxBase control = (ComboBoxBase) o;
                Object value = control.getValue();
                if(value instanceof SelectModel){
                    SelectModel selectModel = (SelectModel) value;
                    jsonObject.put(control.getId(),selectModel.getId());
                }else {
                    jsonObject.put(control.getId(),control.getValue());
                }

            }else if(o instanceof CheckBox){
                CheckBox checkBox = (CheckBox) o;
                if(checkBox.isSelected()){
                    jsonObject.put(checkBox.getId(),1);
                }else {
                    jsonObject.put(checkBox.getId(),0);
                }
            }else if(o instanceof DateTimePicker){
                DateTimePicker dateTimePicker = (DateTimePicker) o;
                LocalDateTime value = dateTimePicker.dateTimeProperty().getValue();
                if(value != null){
                    DateTimeFormatter df = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
                    String format = df.format(value);
                    jsonObject.put(dateTimePicker.getId(),format);
                }else {
                    jsonObject.put(dateTimePicker.getId(),"");
                }

            }else {
                new AppBugExcepiton("字段类型未定义");
            }
        }
        return jsonObject;
    }

    public static void addFieldData(Map data,List<Field> list,Object obj) {
        for (int i = 0; i < list.size(); i++) {
            Field field = list.get(i);
            field.setAccessible(true);
            Object o = null;
            try {
                o = field.get(obj);
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
            if(o instanceof TextInputControl){
                TextInputControl textField = (TextInputControl) o;
                if(StringUtils.isNotEmpty(textField.getId()) && data.get(textField.getId()) != null){
                    if(data.get(textField.getId()) instanceof BigDecimal){//这个地方临时处理下，处理不太妥善
                        BigDecimal o1 = (BigDecimal) data.get(textField.getId());
                        double v = o1.doubleValue();
                        textField.setText(String.valueOf(v));
                    }else {
                        textField.setText(data.get(textField.getId()).toString());
                    }

                }

            }else if(o instanceof QueryComboBox){
                QueryComboBox control = (QueryComboBox) o;
                Object value =o;
                if(value != null){
                    if(StringUtils.isNotEmpty(control.getId()) && data.get(control.getId()) != null){
                        Object o1 = data.get(control.getId());
                        if(o1 instanceof SelectModel){
                            SelectModel selectModel = (SelectModel) o1;
                            control.setSelectData(selectModel);
                        }

                    }
                }
            }else if(o instanceof ComboBoxBase){
                ComboBoxBase control = (ComboBoxBase) o;
                Object value =o;
                if(value != null){
                    if(StringUtils.isNotEmpty(control.getId()) && data.get(control.getId()) != null){
                        if(value instanceof ZymComboBox){
                            ZymComboBox zymComboBox = (ZymComboBox) value;
                            ObservableList<SelectModel> items = zymComboBox.getItems();
                            for (int j = 0; j <items.size() ; j++) {
                                Object o1 = data.get(control.getId());
                                String values =data.get(control.getId()).toString();
                                if(o1 instanceof BigDecimal){
                                    double v = Double.parseDouble(values);
                                    values = String.valueOf(v);
                                }
                                if(items.get(j).getId().equals(values)){
                                    zymComboBox.setValue(items.get(j));
                                }
                            }
                        }else {
                            control.setValue(data.get(control.getId()).toString());
                        }

                    }
                }
            }else if(o instanceof CheckBox){
                CheckBox checkBox = (CheckBox) o;
                if(StringUtils.isNotEmpty(checkBox.getId()) && data.get(checkBox.getId()) != null){
                    String value = data.get(checkBox.getId()).toString();
                    int i1 = Integer.parseInt(value);
                    if(i1 == 0){
                        checkBox.setSelected(false);
                    }else {
                        checkBox.setSelected(true);
                    }
                }

            }else if(o instanceof DateTimePicker){
                DateTimePicker dateTimePicker = (DateTimePicker) o;
                if(StringUtils.isNotEmpty(dateTimePicker.getId()) && data.get(dateTimePicker.getId()) != null){
                    String value = data.get(dateTimePicker.getId()).toString();
                    LocalDateTime localDateTime = LocalDateTime.parse(value, DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
                    dateTimePicker.dateTimeProperty().set(localDateTime);//设置DateTimePicker要显示的时间
                }else {
                    dateTimePicker.dateTimeProperty().set(null);
                }

            }else {
                new AppBugExcepiton("字段类型未定义");
            }
        }
    }
    public static void clearFieid(List<Field> fields, Object controller) {
        for (int i = 0; i < fields.size(); i++) {
            Field field = fields.get(i);
            field.setAccessible(true);
            Object o = null;
            try {
                o = field.get(controller);
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
            if(o instanceof NumberTextField){
                NumberTextField textField = (NumberTextField) o;
                textField.setText("0");
            }else if(o instanceof TextInputControl) {
                TextInputControl control = (TextInputControl) o;
                control.setText(null);
            }else if(o instanceof ComboBoxBase){
                ComboBoxBase control = (ComboBoxBase) o;
                control.setValue(null);
            }else if(o instanceof DateTimePicker){
                DateTimePicker control = (DateTimePicker) o;
                control.dateTimeProperty().setValue(LocalDateTime.now());
            }else {
                new AppBugExcepiton("字段类型未定义");
            }
        }
    }
    public static Node getFieidNode(Field field, Object obj){
        try {
            field.setAccessible(true);
            return (Node) field.get(obj);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        return null;
    }



    public static void clearError(Field field, Object controller) {
        Node fieidNode = FromUtils.getFieidNode(field, controller);
        Parent parent1 = fieidNode.getParent();
        List<Integer> removeIndex = new ArrayList<>();
        if(parent1 instanceof Pane){
            Pane parent = (Pane) parent1;
            ObservableList<Node> children = parent.getChildren();
            for (int i = 0; i < children.size(); i++) {
                Node node = children.get(i);
                node.setStyle("");
                if(StringUtils.isNotEmpty(node.getId()) && node.getId().indexOf("#_Error") >= 0){
                    removeIndex.add(i);
                }
            }
            Collections.reverse(removeIndex);
            for (int i = 0; i <removeIndex.size() ; i++) {
                children.remove(removeIndex.get(i).intValue());
            }
        }
    }

    public static void initDataBook(String key, ComboBox comboBox) {
        JSONObject fylx = dataBook.getJSONObject(key);
        for (Map.Entry<String, Object> m : fylx.entrySet()) {
            SelectModel selectModel = new SelectModel();
            selectModel.setId(m.getKey());
            selectModel.setValue(m.getValue().toString());
            comboBox.getItems().add(selectModel);
        }
    }
    public static void initDataBookDefault(String key, ComboBox comboBox) {
        initDataBook(key,comboBox);
        if(comboBox.getItems().size() >0){
            comboBox.setValue(comboBox.getItems().get(0));
        }

    }
}
