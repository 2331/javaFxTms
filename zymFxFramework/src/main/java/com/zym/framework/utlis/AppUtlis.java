package com.zym.framework.utlis;

import javafx.application.Platform;
import javafx.geometry.Orientation;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.SplitPane;
import javafx.stage.Modality;
import javafx.stage.Stage;

public abstract class AppUtlis {

    public static void runLater(Runnable runnable) {
        if (Platform.isFxApplicationThread()) {
            runnable.run();
        } else {
            Platform.runLater(runnable);
        }

    }
    public static void systemOut(){
        Platform.exit();
        System.exit(0);
    }

    /**
     * 创建新窗口
     * @return
     */
    public static Stage getFromWinView(String title, Parent root){
        Stage newStage = new Stage();
        newStage.initModality(Modality.APPLICATION_MODAL);
        newStage.setTitle(title);
        newStage.setResizable(true);

        SplitPane splitPane = new SplitPane();
        splitPane.setOrientation(Orientation.VERTICAL);
        splitPane.getItems().add(root);
        //创建布局
        Scene scene = new Scene(splitPane);
        newStage.setScene(scene);
        return newStage;
    }


}