package com.zym.framework.utlis;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.zym.application.dataBook.DataBookUtils;
import com.zym.application.utils.StringUtils;
import com.zym.framework.constants.Grid;
import com.zym.framework.grid.CheckCell;
import com.zym.framework.grid.DataBookCell;
import com.zym.framework.grid.IDCell;
import com.zym.framework.grid.JSONObjectCell;
import com.zym.framework.model.ParseModel;
import com.zym.framework.model.SelectModel;
import javafx.scene.control.CheckBox;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.util.Callback;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * QQ 165149324
 * 臧英明
 *
 * @author
 * @create 2021-02-05
 */
public abstract class IviewParseUtils extends HtmlParseUtils {


    public static ParseModel parseGrid(String jsonData, Map map){
        ParseModel parseModel = new ParseModel();
        List<TableColumn> list =new ArrayList<>();
        List<SelectModel> selectModels =new ArrayList<>();
        JSONArray jsonArray = JSONArray.parseArray(jsonData);
        for (int i = 0; i <jsonArray.size() ; i++) {
            JSONObject jsonObject = jsonArray.getJSONObject(i);
            TableColumn tableColumn = new TableColumn();
            String type = jsonObject.getString("type");
            String key = jsonObject.getString("key");
            //查询模型
            if(jsonObject.containsKey("query") || jsonObject.containsKey("queryIndex")){
               if("text".equals(jsonObject.getString("query"))){
                   SelectModel selectModel = new SelectModel();
                   selectModel.setValue( jsonObject.getString("title"));
                   if(StringUtils.isEmpty(jsonObject.getString("queryIndex"))){
                       selectModel.setId(jsonObject.getString("key"));
                   }else {
                       selectModel.setId(jsonObject.getString("queryIndex"));
                   }
                   selectModels.add(selectModel);
               }else {
                   SelectModel selectModel = new SelectModel();
                   selectModel.setValue( jsonObject.getString("title"));
                   if(StringUtils.isEmpty(jsonObject.getString("queryIndex"))){
                       selectModel.setId(jsonObject.getString("key"));
                   }else {
                       selectModel.setId(jsonObject.getString("queryIndex"));
                   }
                   selectModels.add(selectModel);
               }
            }
            if(StringUtils.isNotEmpty(type) && "selection".equals(type)){
                CheckBox checkBox = new CheckBox();
                checkBox.setId("$_SELECT_CHECKBOX");
                tableColumn.setGraphic(checkBox);
                tableColumn.setPrefWidth(45);
                tableColumn.setId(Grid.SELECT);
                tableColumn.setCellValueFactory(new JSONObjectCell<>(Grid.SELECT));
                tableColumn.setCellFactory(new CheckCell());
            }else if(StringUtils.isNotEmpty(type) && "index".equals(type)){
                tableColumn.setId(Grid.INDEX);
                tableColumn.setText("序号");
                tableColumn.setPrefWidth(50);
                tableColumn.setStyle("-fx-alignment: center;");
                tableColumn.setCellFactory(new IDCell<>());
            }else if(StringUtils.isNotEmpty(key) && map != null&& map.containsKey(key)){
                //数据字典
                tableColumn.setId(jsonObject.getString("key"));
                tableColumn.setText(jsonObject.getString("title"));
                tableColumn.setPrefWidth(jsonObject.getIntValue("width"));
                if(jsonObject.getIntValue("width") == 0){
                    tableColumn.setPrefWidth(230);
                }
                //获取数据字典
                tableColumn.setCellValueFactory(new JSONObjectCell<String>(key));
                JSONObject dataBookValue = DataBookUtils.getDataBookValue(map.get(key).toString());
                Object value = map.get(key);
                if(value instanceof Callback){ //如果是cellback 则直接传入
                    tableColumn.setCellValueFactory((Callback<TableColumn, TableCell>) value);
                }else {
                    tableColumn.setCellFactory(new DataBookCell(dataBookValue,key));
                }

            }else {
                tableColumn.setId(jsonObject.getString("key"));
                tableColumn.setText(jsonObject.getString("title"));
                tableColumn.setPrefWidth(jsonObject.getIntValue("width"));
                if(jsonObject.getIntValue("width") == 0){
                    tableColumn.setPrefWidth(230);
                }
                tableColumn.setCellValueFactory(new JSONObjectCell<String>(jsonObject.getString("key")));
            }

            list.add(tableColumn);
        }
        parseModel.setTableColumns(list);
        parseModel.setSelectModels(selectModels);
        return parseModel;
    }


}
