package com.zym.framework;

import com.alibaba.fastjson.JSONObject;

/**
 * QQ 165149324
 * 臧英明
 *
 * @author
 * @create 2021-03-08
 */
public interface CallBack {
    void run(JSONObject jsonObject);
}
