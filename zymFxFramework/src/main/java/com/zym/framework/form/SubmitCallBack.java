package com.zym.framework.form;

import com.alibaba.fastjson.JSONObject;

/**
 * QQ 165149324
 * 臧英明
 *
 * @author
 * @create 2021-03-01
 */
public interface SubmitCallBack {
    void dataUpdate(JSONObject jsonObject);
}
