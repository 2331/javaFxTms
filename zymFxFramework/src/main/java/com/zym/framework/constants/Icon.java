package com.zym.framework.constants;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

/**
 * QQ 165149324
 * 臧英明
 *
 * @author
 * @create 2021-02-24
 */
public class Icon {
    public static final  ImageView GAODE = getIcon("");

    public static ImageView getIcon(String path){

        ImageView imageView = new ImageView();
        imageView.setFitHeight(20);
        imageView.setFitWidth(20);
        imageView.setImage(new Image(path));
        return imageView;
    }
}
