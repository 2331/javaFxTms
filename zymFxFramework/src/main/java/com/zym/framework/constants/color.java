package com.zym.framework.constants;

/**
 * QQ 165149324
 * 臧英明
 *
 * @author
 * @create 2021-01-31
 */
public interface color {
    //错误文字颜色
    String ERROR_COLOR = "#ed321f";
    //通用红色
    String RED = "#ed321f";
    //通用绿色
    String GREEN = "#047C3E";
}
