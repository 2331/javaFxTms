package com.zym.framework.winTool;

import com.zym.framework.CallBack;
import com.zym.framework.model.SelectModel;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.CheckBox;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

/**
 * QQ 165149324
 * 臧英明
 *
 * @author
 * @create 2021-03-11
 */
public class WinCheck extends AnchorPane implements Initializable {
    private Stage stage;
    private FXMLLoader fxmlLoader;
    @FXML
    private VBox vBox;
    private CallBack callBack;
    public WinCheck(CallBack callBack){
        this.callBack = callBack;
        // Load FXML
        fxmlLoader = new FXMLLoader(
                getClass().getResource("/component/winCheck/winCheck.fxml"));
        fxmlLoader.setRoot(this);
        fxmlLoader.setController(this);

        try {
            fxmlLoader.load();
        } catch (IOException ex) {
            // Should never happen.  If it does however, we cannot recover
            // from this
            throw new RuntimeException(ex);
        }
//        fxmlLoader = FxmlUtil.loadFxmlFromResource("/component/winCheck/winCheck.fxml");
//        fxmlLoader.setRoot(this);
//        fxmlLoader.setController(this);

    }
    public void add(SelectModel selectModel){
        CheckBox checkBox = new CheckBox(selectModel.getValue());
        checkBox.setId(selectModel.getId());
        vBox.getChildren().add(checkBox);
    }
    public void addAll(List<SelectModel> list){
        for (int i = 0; i <list.size() ; i++) {
            CheckBox checkBox = new CheckBox(list.get(i).getValue());
            checkBox.setId(list.get(i).getId());
            vBox.getChildren().add(checkBox);
        }
    }
    public void setAll(List<SelectModel> list){
        vBox.getChildren().clear();
        for (int i = 0; i <list.size() ; i++) {
            CheckBox checkBox = new CheckBox(list.get(i).getValue());
            checkBox.setPadding(new Insets(6,0,0,10));
            checkBox.setId(list.get(i).getId());
            vBox.getChildren().add(checkBox);
        }
    }
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        stage = new Stage();
        stage.setScene(new Scene(fxmlLoader.getRoot()));
        stage.setTitle("请选择");
    }
    public void show(){
        stage.show();
    }
    public void Hide(){
        stage.hide();
    }
    public  List<String> getIds(){
        List<String> ids = new ArrayList<>();
        ObservableList<Node> children = this.vBox.getChildren();
        for (int i = 0; i <children.size() ; i++) {
            CheckBox node = (CheckBox) children.get(i);
            if(node.isSelected()){
                ids.add(children.get(i).getId());
            }

        }
        return ids;
    }
    public void save(){
        callBack.run(null);
    }
}
