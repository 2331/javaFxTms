package com.zym.framework.winTool;

import com.zym.application.utils.StringUtils;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ToolBar;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

/**
 * QQ 165149324
 * 臧英明
 *
 * @author
 * @create 2021-03-15
 */
public class WinTool extends AnchorPane implements Initializable {
    private Stage stage;
    private FXMLLoader fxmlLoader;
    private String title = "请选择";
    @FXML
    private AnchorPane pane;
    @FXML
    private ToolBar toolBar;
    public WinTool(){
        this(null);
    }
    public WinTool(String title){
        if(StringUtils.isNotEmpty(title)){
            this.title = title;
        }
        fxmlLoader = new FXMLLoader(
                getClass().getResource("/component/winTool/winTool.fxml"));
        fxmlLoader.setRoot(this);
        fxmlLoader.setController(this);

        try {
            fxmlLoader.load();
        } catch (IOException ex) {
            throw new RuntimeException(ex);
        }
    }
    public void setNode(Node node){
        pane.getChildren().setAll(node);
    }
    public void setButton(Button button){
        toolBar.getItems().add(button);
    }
    public void setHeightWidth(int height,int width){
        pane.setPrefHeight(height);
        pane.setPrefWidth(width);
        toolBar.setLayoutY(height-2);
        toolBar.setPrefWidth(width);
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        stage = new Stage();
        stage.setScene(new Scene(fxmlLoader.getRoot()));
        stage.setTitle(title);
    }
    public void show(){
        stage.show();
    }
    public void Hide(){
        stage.hide();
    }
}
