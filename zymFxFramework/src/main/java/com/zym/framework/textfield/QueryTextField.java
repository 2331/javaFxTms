package com.zym.framework.textfield;

import com.zym.framework.dialog.FxDialog;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Parent;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;

import java.util.List;

/**
 * QQ 165149324
 * 臧英明
 * 查询
 *
 * @author
 * @create 2021-03-02
 */
public class QueryTextField extends TextField {
    private TableView table;
    private ObservableList data = FXCollections.observableArrayList();
    private List<TableColumn> tableColumns;
    FxDialog<Void> fxDialog = new FxDialog<Void>();

    public QueryTextField(){

    }
    public void initGrid(List<TableColumn> tableColumns){
         this.tableColumns = tableColumns;
         table = new TableView();
         table.setItems(data);
         table.getColumns().setAll(tableColumns);
         table.setLayoutX(this.getLayoutX());
         table.setLayoutY(this.getLayoutY()-this.getHeight());
         Parent parent = this.getParent();

    }
}
