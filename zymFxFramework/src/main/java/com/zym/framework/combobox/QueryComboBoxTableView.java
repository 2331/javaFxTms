package com.zym.framework.combobox;

import com.alibaba.fastjson.JSONObject;
import com.sun.javafx.scene.control.behavior.ComboBoxBaseBehavior;
import com.sun.javafx.scene.control.behavior.ComboBoxListViewBehavior;
import com.sun.javafx.scene.control.skin.ComboBoxPopupControl;
import com.zym.framework.model.SelectModel;
import javafx.beans.value.ObservableValue;
import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.util.Callback;
import javafx.util.StringConverter;

import java.util.List;

/**
 * QQ 165149324
 * 臧英明
 *
 * @author
 * @create 2021-03-02
 */
public class QueryComboBoxTableView extends ComboBoxPopupControl<SelectModel> {
    private ComboBox comboBox;

    private final TableView table = new TableView();
    private final ObservableList<JSONObject> data =table.getItems();

    private ListCell<SelectModel> buttonCell;
    public QueryComboBoxTableView(final ComboBox<SelectModel> comboBox) {
        super(comboBox, new ComboBoxListViewBehavior<SelectModel>(comboBox));
        this.comboBox = comboBox;
        getChildren().add(table);
        comboBox.focusedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            if(newValue){
                show();
            }else {
                hide();
            }
        });
    }
    public QueryComboBoxTableView(ComboBoxBase<SelectModel> comboBoxBase, ComboBoxBaseBehavior<SelectModel> behavior) {
        super(comboBoxBase, behavior);
    }
    public void setColumns(List<TableColumn> tableColumns){
        table.getColumns().setAll(tableColumns);
    }
    public void setRowDbOnlick(Callback<TableView, TableRow> callback){
        table.setRowFactory(callback);
    }
    @Override
    protected Node getPopupContent() {
        return table;
    }

    @Override
    protected TextField getEditor() {
        return getSkinnable().isEditable() ? ((ComboBox)getSkinnable()).getEditor() : null;
    }

    @Override
    protected StringConverter<SelectModel> getConverter() {
        return ((ComboBox)getSkinnable()).getConverter();
    }

    @Override
    public Node getDisplayNode() {
        Node displayNode;
        if (comboBox.isEditable()) {
            displayNode = getEditableInputNode();
        } else {
            displayNode = buttonCell;
        }

        updateDisplayNode();

        return displayNode;
    }

    public void setData(List list) {
        data.setAll(list);
    }
}
