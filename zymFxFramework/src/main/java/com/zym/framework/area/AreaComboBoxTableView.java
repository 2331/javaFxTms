package com.zym.framework.area;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.sun.javafx.scene.control.behavior.ComboBoxBaseBehavior;
import com.sun.javafx.scene.control.behavior.ComboBoxListViewBehavior;
import com.sun.javafx.scene.control.skin.ComboBoxPopupControl;
import com.zym.application.model.ReturnModel;
import com.zym.application.session.Http;
import com.zym.application.session.HttpCall;
import com.zym.application.utils.StringUtils;
import com.zym.framework.model.SelectModel;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.input.MouseEvent;
import javafx.util.Callback;
import javafx.util.StringConverter;

import java.util.ArrayList;
import java.util.List;

/**
 * QQ 165149324
 * 臧英明
 *
 * @author
 * @create 2021-03-02
 */
public class AreaComboBoxTableView extends ComboBoxPopupControl<SelectModel> {
    private ComboBox comboBox;
    private TreeView table;

    private ListCell<SelectModel> buttonCell;

    public AreaComboBoxTableView(final ComboBox comboBox) {
        super(comboBox, new ComboBoxListViewBehavior<SelectModel>(comboBox));
        this.comboBox = comboBox;
        this.table = new TreeView();
        System.out.println(comboBox.getPrefWidth());
        this.table.setPrefWidth(300);
        getChildren().add(table);
        table.setCellFactory(new Callback<TreeView<SelectModel>,TreeCell<SelectModel>>(){
            @Override
            public TreeCell<SelectModel> call(TreeView<SelectModel> p) {
                return new TreeCell<SelectModel>(){
                    @Override
                    public void updateItem(SelectModel item, boolean empty) {
                        super.updateItem(item, empty);
                        if (empty) {
                            setText(null);
                            setGraphic(null);
                        } else {
                            setId(item.getId());
                            setText(item.getValue());
                            setGraphic( getTreeItem().getGraphic());
                        }
                    }
                };
            }
        });
        initData();

    }
    public void setTableWidth(double value){
        table.setPrefWidth(value);
    }

    public TreeView getTable() {
        return table;
    }

    public void setOnMouseClickedsetOnMouseClicked(EventHandler<MouseEvent> eventHandler){
        table.setOnMouseClicked(eventHandler);
    }

    public void getLeftTree(TreeItem<SelectModel> item){
        SelectModel value = item.getValue();
        if(StringUtils.isNotEmpty(value.getId())){
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("id",value.getId());
            ReturnModel returnModel = Http.postNotProgress("/baseZone/getZoneTree", jsonObject);
            Object obj = returnModel.getObj();
            if(obj != null){
                JSONArray jsonArray = (JSONArray) obj;
                List list = new ArrayList<>();
                for (int i = 0; i < jsonArray.size(); i++) {
                    JSONObject obj2 = jsonArray.getJSONObject(i);
                    TreeItem treeItem = new TreeItem();
                    SelectModel itemModel = new SelectModel();
                    itemModel.setId(obj2.getString("id"));
                    itemModel.setValue(obj2.getString("title"));
                    itemModel.setLevel(item.getValue().getLevel()+1);
                    treeItem.setValue(itemModel);
                    list.add(treeItem);
                }
                item.getChildren().setAll(list);
                item.setExpanded(true);
            }
        }
    }
    private void initData() {
        Http.post("/baseZone/getZoneTree", null, new HttpCall() {
            @Override
            public void exe(ReturnModel returnModel) {
                Object obj = returnModel.getObj();
                if(obj != null){
                    TreeItem<SelectModel> root = new TreeItem();
                    SelectModel rootModel = new SelectModel();
                    rootModel.setId("ROOT");
                    rootModel.setValue("  清空选择");
                    root.setValue(rootModel);
                    root.setExpanded(true);
                    JSONArray jsonArray = (JSONArray) obj;
                    for (int i = 0; i < jsonArray.size(); i++) {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        TreeItem treeItem = new TreeItem();
                        SelectModel itemModel = new SelectModel();
                        itemModel.setId(jsonObject.getString("id"));
                        itemModel.setValue(jsonObject.getString("title"));
                        treeItem.setValue(itemModel);
                        root.getChildren().add(treeItem);
                    }
                    table.setRoot(root);
                }
            }
        });
    }

    public AreaComboBoxTableView(ComboBoxBase<SelectModel> comboBoxBase, ComboBoxBaseBehavior<SelectModel> behavior) {
        super(comboBoxBase, behavior);
    }


    @Override
    protected Node getPopupContent() {
        return table;
    }

    @Override
    protected TextField getEditor() {
        return getSkinnable().isEditable() ? ((ComboBox) getSkinnable()).getEditor() : null;
    }

    @Override
    protected StringConverter<SelectModel> getConverter() {
        return ((ComboBox) getSkinnable()).getConverter();
    }

    @Override
    public Node getDisplayNode() {
        Node displayNode;
        if (comboBox.isEditable()) {
            TextField editableInputNode = getEditableInputNode();
            editableInputNode.setDisable(true);
            editableInputNode.setStyle("-fx-opacity: 1.0;"); //干掉禁用样式

            displayNode = editableInputNode;
        } else {
            displayNode = buttonCell;
        }
        updateDisplayNode();

        return displayNode;
    }
}